require 'nokogiri'
require 'spreadsheet'
require 'pry-byebug'

regexp_columns = /(.*):/
regexp_columns_content = /: (.*)/
files = File.join("**", "*.html")
leads_html_files = Dir.glob(files)
@file ="leads.xls"

def columns(doc)
    if t_parsed = doc.xpath("//*[@id='msg_body']").text
    elsif
        t_parsed = doc.xpath("//html/body/div/p[12]").text
    else
        t_parsed = doc.xpath("//*[@id='header_row_even']/td[2]").text
    end
    t_parsed
end

leads_html_files.each_with_index do |f, index|
    begin
        html_files = File.open(f)
        document = Nokogiri::HTML(html_files) do |config|
            config.strict.nonet
        end
        @cn =  columns(document)
        @b_columns = @cn.scan(regexp_columns)
        @columns_content = @cn.scan(regexp_columns_content)
        @column_subject = document.xpath("//*[@id='header_row_even']/td[2]").text
        b = @columns_content.flatten
        b.push(@column_subject)
        a = @b_columns.flatten

        if File.exist? @file
            Spreadsheet.client_encoding = 'UTF-8'
            open_book = Spreadsheet.open @file
            new_row_index = open_book.worksheet(0).last_row_index + 1
            p "Inserting at this position: #{new_row_index}"
            open_book.worksheet(0).insert_row(new_row_index, b)
            File.delete @file
            open_book.write @file
        else
            Spreadsheet.client_encoding = 'UTF-8'
            @book = Spreadsheet::Workbook.new
            @book.create_worksheet :name => 'leads_spreadsheet'
            @book.worksheet(0).insert_row 0, a
            @book.write @file
        end
    rescue
        p "Unexpected Error!"
    ensure
        html_files.close unless html_files.nil?
    end
end